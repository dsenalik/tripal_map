<?php

/*
 * File: mainlab_feature_QTL_map_positions.tpl.php
 * From MainLab Tripal extension module for Tripal that includes custom tables for Chado used 
 * by the Main Bioinformatics Laboratory. Version core = 7.x.
 * Modified for tripal_map to display link to MapViewer page from the feature Map Positions pane.
*/

$feature = $variables['node']->feature;
$map_positions = $feature->mainlab_qtl->map_positions;
$counter_pos = count($map_positions);

if ($counter_pos > 0) {
  $cmap_enabled = variable_get('mainlab_tripal_cmap_links', 1);
  $header = array ('#', 'Map Name', 'Linkage Group', 'Bin', 'Chromosome', 'Peak', 'Span Start', 'Span Stop', 'MapViewer');
  if ($cmap_enabled) {
    $header[] = 'CMap';
  }

  $feature_prop = property_exists($feature, 'featureprop') ? $feature->featureprop : array();
  $num_feature_prop = count($feature_prop);
  $qtl_id = -1;
  if ($num_feature_prop > 0) {
    $feature_id = property_exists($feature_prop[0], 'feature_id') ? $feature_prop[0]->feature_id : 0;
    if ($feature_id) {
      $qtl_id = property_exists($feature_id, 'feature_id') ? $feature_id->feature_id : -1;
    }
  }

  $rows = array ();
  $counter = 1;
  foreach($map_positions AS $pos) {
    $link = tripal_map_mainlab_tripal_link_record('featuremap', $pos->featuremap_id);
    $map = $link ? "<a href=\"$link\">$pos->name</a>" : $pos->name;
    $lg = $pos->linkage_group ? $pos->linkage_group : "N/A";
    $bin = $pos->bin ? $pos->bin : "N/A"; 
    $chr = $pos->chr ?$pos->chr : "N/A";
    $start = $pos->qtl_start || $pos->qtl_start === '0' ? round($pos->qtl_start, 2) : '-';
    $stop = $pos->qtl_stop || $pos->qtl_stop === '0'  ? round($pos->qtl_stop, 2) : '-';
    $peak = $pos->qtl_peak || $pos->qtl_peak  === '0' ? round($pos->qtl_peak, 2) : '-';
    $highlight = $node->feature->uniquename;

    $linkage_group = $pos->linkage_group;
    if ($linkage_group) {
      $linkage_group = str_replace("/", "_forwardslash_", $linkage_group);
      $linkage_group = str_replace("+", "_plussign_", $linkage_group);
    }
    else {
      $linkage_group = "";
    }
    
    $mapviewer = (!$pos->featuremap_id || !$pos->linkage_group)? "N/A" : "<a href=\"/mapviewer/$pos->featuremap_id".
      "/$linkage_group/$qtl_id\" target=\"_blank\">View</a>";

    if ($cmap_enabled) {
      $cmap = (!$pos->urlprefix || !$pos->accession)? "N/A" : "<a href=\"$pos->urlprefix$pos->accession" . 
      "&ref_map_acc=-1&highlight=" . $highlight . "\">View</a>";
      $rows[] = array ($counter, $map, $lg, $bin, $chr, $peak, $start, $stop, $mapviewer, $cmap);
    }
    else {
      $rows[] = array ($counter, $map, $lg, $bin, $chr, $peak, $start, $stop, $mapviewer);
    }
    
    $counter ++;
  }
  $table = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(
      'id' => 'tripal_feature_qtl-table-map-positions',
    ),
    'sticky' => FALSE,
    'caption' => '',
    'colgroups' => array(),
    'empty' => '',
  );
  print theme_table($table);
} ?>

