
////////////////////////////////////////////////////////////
// Display correspondence matrix with D3.js

var dotplot = dotplot || {};

dotplot = {
		
	drawDotplotPlotly: function(primMapId, primMap, primLinkageGroup, secMapId, secMap, secLinkageGroup, corres ) {
		
		// first obtain the correspondence datapoints
		dpHash = {};
		for (var ckey in corres) {
			//corres: {"X17_1500":[{"linkageGroup":"LGIII","position":"184.2", "feature_id": 1233554},{"linkageGroup":"LGIIIb.3","position":"74.5943", ..}],"
			if (corres.hasOwnProperty(ckey)) {
				markerCorres = corres[ckey];
				if (Object.keys(markerCorres).length >= 2 ) {
					// only use the marker if it pertains to either the primary or secondary linkage group (correspondences are for the whole map)
					//if ((markerCorres[0].linkageGroup == primLinkageGroup) || (markerCorres[0].linkageGroup == secLinkageGroup)) {
					var xPos = parseFloat(markerCorres[0].position);
					var yPos = parseFloat(markerCorres[1].position);
					if (!(yPos in dpHash)) {
						dpHash[yPos] = {};
						dpHash[yPos][xPos] = {};
						dpHash[yPos][xPos][ckey] = markerCorres[0];
					}
					else {
						if (!(xPos in dpHash[yPos])) {
							dpHash[yPos][xPos] = {};
							dpHash[yPos][xPos][ckey] = markerCorres[0];
						} 
						else {
							dpHash[yPos][xPos][ckey] = markerCorres[0];
						}
					}
				}
			}
		};

		// reorder the points where duplication of position occurs
		for (var y in dpHash) {
			for (var x in dpHash[y]) {
				if (Object.keys(dpHash[y][x]).length > 1) {
					// find a new position for this data point, other markers exist with the same coords.
					var count = 1;
					for (var point in dpHash[y][x]) {
						// skip the first point, leaving one in its existing position and reposition the others.
						if (count > 1) {
							dpHash = dotplot.findNewPos(dpHash, x, y, point);
						}
						count += 1;
					}
				}
			}
		}

		// add the points to the data point structure to pass to the scatter plot
		var dp = {};
		dp['x'] = [];
		dp['y'] = [];
		dp['label'] = [];
		dp['annotation'] = [];
		for (var yPos in dpHash) {
			for (var xPos in dpHash[yPos]) {
				for (var ckey in dpHash[yPos][xPos]) {
					dp.x.push(xPos);
					dp.y.push(yPos);
					dp.label.push(ckey);
					dp.annotation.push({ x: xPos, y: yPos, text: "<a href = '"+Drupal.settings.baseUrl+dpHash[yPos][xPos][ckey].feature_url+"'>{}</a>",
						showarrow: false, xanchor:'center', yanchor:'center', opacity: 0, hovertext: ckey });
				}
			}
		}

		var markerSize = 10;
		var trace1 = {
			x: dp.x,
			y: dp.y,
			mode: 'markers',
			type: 'scatter',
			//text: dp.label, use annotation with hover over option instead of labels, so can specify a link.
			marker: {
				size: markerSize,
				color: 'rgb(58, 118, 175)',
				line: { color: 'rgb(128, 158, 193)', width: 1},
			},
			//hoverinfo: "text", - default for hoverinfo, is to show position and text if available. If do not want position to appear, specifiy: none.
			cliponaxis: false,
		};

		var data = [ trace1];

		var svgWidth = 600;
		var svgHeight = 600;
		var fontSize = 12;

		var maxLineLenX = Math.floor(svgWidth/fontSize);
		var xAxisMapTitle = dotplot.formatAxisName(primMap, maxLineLenX);
		var xAxisLgTitle = tripalMap.lGNameReduction(primLinkageGroup, primMap);

		var maxLineLenY = Math.floor(svgHeight/fontSize);
		var yAxisMapTitle = dotplot.formatAxisName(secMap, maxLineLenY);
		var yAxisLgTitle = tripalMap.lGNameReduction(secLinkageGroup, secMap);
		
		var layout = {
			
			width: svgWidth, 
	        height: svgHeight, 
			xaxis: {
				range: [0, (Math.max(...dp.x) + markerSize)],
				title: "<a href = '"+Drupal.settings.baseUrl+ "/mapviewer/" + primMapId + "/" 
				+ encodeURI(primLinkageGroup.replace(/\//g, "_forwardslash_").replace(/\+/g, "_plussign_")) + "'>"
				+ xAxisMapTitle + "<br>" + xAxisLgTitle + "</a>",
				layer: "below traces", 
			},

			yaxis: {
				range: [0, (Math.max(...dp.y) + markerSize)],
				title: "<a href = '"+Drupal.settings.baseUrl+ "/mapviewer/" + secMapId + "/" 
				+ encodeURI(secLinkageGroup.replace(/\//g, "_forwardslash_").replace(/\+/g, "_plussign_")) + "'>"
				+ yAxisMapTitle + "<br>" + yAxisLgTitle + "</a>",
				layer: "below traces",

			},
			hovermode:'closest',
			showlegend: false,
			annotations: dp.annotation, 
		};
		
		Plotly.newPlot('select_fieldset_dotplot_svg', data, layout);
	},
	
	formatAxisName: function(axisName, maxLen) {
		// return name, tokenized by the break character, where each token does not exceed maxLen
		var newStr = '';

		let aName = axisName;
		var numLines = 1;
		if (axisName.length > maxLen) {
			// the name will not fit on one line. Segment it.
			var line = '';
		    for (let character of aName) {
				line += character;
		    	if (line.length >= maxLen) {
		    		// write the line, purge the line buffer
		    		newStr += line;
					line = '';
					if (numLines >= 3) {
	    				// only three lines will fit on the axes label. append an ellipses and break
	    				newStr += '...';
						numLines += 1;
	    				break;
	    			}
		    		if (axisName.length > newStr.length) {
		    			// append the line break only if this is not the last line
		    			newStr += '<br>';
					}
					numLines += 1;
		    	}
		    }
		    newStr += line;
		}
		else {
			// the original string will fit on one line
			newStr = axisName;
		}
		
	    return newStr;
	},
	
	findNewPos: function(dpHash, x, y, point) {
		// perturb the position slightly until unique position found, then remove old position.
		var incr = 0.2;
		var mult = 1.0;
		x = parseFloat(x); // cast the position string to a float
		y = parseFloat(y); // cast the position string to a float
		var newX = x;
		var newY = y;
		while (dotplot.pointExists(newX, newY, dpHash) && (mult <= 5)) {
			newX = x + incr*mult;
			if (!dotplot.pointExists(newX, y, dpHash)) { break;}
			if (!dotplot.pointExists(newX, y + incr*mult, dpHash)) { newY = y + incr*mult; break;}
			if (!dotplot.pointExists(newX, y - incr*mult, dpHash)) { newY = y - incr*mult; break;}
			newX = x - incr*mult;
			if (!dotplot.pointExists(newX, y, dpHash)) { break;}
			if (!dotplot.pointExists(newX, y + incr*mult, dpHash)) { newY = y + incr*mult; break;}
			if (!dotplot.pointExists(newX, y - incr*mult, dpHash)) { newY = y - incr*mult; break;}
			newX = x;
			if (!dotplot.pointExists(x, y + incr*mult, dpHash)) { newY = y + incr*mult; break;}
			if (!dotplot.pointExists(x, y - incr*mult, dpHash)) { newY = y - incr*mult; break;}
			mult = mult+1; // stop after 5 tries
		}
		// add new pos
		if (!(newY in dpHash)) {
			dpHash[newY] = {};
		}
		dpHash[newY][newX] = {};
		dpHash[newY][newX][point] = dpHash[y][x][point];
		// remove the old pos
		if (y in dpHash) {
			if (x in dpHash[y]) {
				if (point in dpHash[y][x]) {
					delete dpHash[y][x][point];
				}
			}
		}
		return dpHash;
	},
	
	pointExists: function(x, y, dpHash) {
		exists = false;
		if (y in dpHash) {
			if (x in dpHash[y]) {
				exists = true;
			}
		}
		return exists;
	},
	
	drawDotplot: function(primMap, primLinkageGroup, secMap, secLinkageGroup, corres ) {
		// return if there is no map
		if ((primMap === null ) || (secMap === null)) { 
			return;
		}

		var margin = {top: 75, right: 0, bottom: 0, left: 100};
	    var width = 600;
	    var height = 900;

		var svg = d3.select("#select_fieldset_dotplot_svg")
			.append("svg")
			.attr("class", "TripalMap")
	    	.attr("width", width)
	    	.attr("height", height);
			
		// correspondence matrix frame 
	    var cmFrame = svg.append("g")
        	.attr("id", "cmFrame")
        	.attr("transform", "translate("+ margin.left + "," + margin.top + ")")
        	.attr("visibility", "unhidden");
	    
	    // parse correspondences and put them into format: [{"x":67.91,"y":39.57},...]
	    var dp = [];
	    for (var ckey in corres) {
	    	//corres: {"X17_1500":[{"linkageGroup":"LGIII","position":"184.2"},{"linkageGroup":"LGIIIb.3","position":"74.5999999999999943"}],"
	    	if (corres.hasOwnProperty(ckey)) {
				markerCorres = corres[ckey];
				if (Object.keys(markerCorres).length >= 2 ) {
					var corresVal = {};
					var xPos = parseInt(markerCorres[0].position);
					corresVal["x"] = xPos;
					var yPos = parseInt(markerCorres[1].position);
					corresVal["y"] = yPos;
					dp.push(corresVal);
				}
	    	}
	    };
	    
        var options = {"dp": dp, "primMap": primMap, "primLinkageGroup": primLinkageGroup, 
        		"secMap": secMap, "secLinkageGroup": secLinkageGroup, "corres": corres};

        dotplot.draw(options, cmFrame);
	},
        
	draw: function(options, svg) {

		var data = options.dp;
    	var width = 250;
    	var height = 250;
    	var textColor = "#3b3b3b";
    	        	
    	if(!data) {
    		throw new Error('Please pass data');
    	}

    	// Create x label
    	var xValue = function(d) { return d.x;};
    	var xMin = d3.min(data, xValue);
    	var xMax = d3.max(data, xValue);
		var mapX = options.primMap; 
    	var lgX = options.primLinkageGroup;
    	lgX = tripalMap.lGNameReduction(lgX, mapX);
    	var xLabel = [mapX, lgX, "0 -"+xMax+" cM"];
    	
    	// x-axis
       	var xScale = d3.scaleLinear().range([0, width]);
    	// prevent dots overlapping the axis. Add buffer to data domain
    	var padding = 6;
    	xScale.domain([d3.min(data, xValue) - padding, d3.max(data, xValue) + padding]);

    	var xAxis = '';
    	if (tripalMap.d3VersionFour()) { 
    		xAxis = d3.axisTop(xScale);
    	}
    	else {
    		xAxis = d3.svg.axis().scale(xScale).orient("top");
    	}
    	xAxisSvg = svg.append("g")
        	.attr("class", "x axis");
    	xAxisSvg.call(xAxis);

    	var xAxisg = xAxisSvg.append("g")
       		.attr("fill", textColor)
    		.attr("transform", "translate(45,-60)");

    	xAxisg.selectAll("text")
			.data(xLabel)
    		.enter().append("text")
    		.attr("class", "label").attr("y", function(d,i) { return (i*18);})
 			.style("text-anchor", "start").style("font-size", "15px").style("line-height", "18px")
    		.text(function(d) {return d;});
    	
    	// Create y label
    	var yValue = function(d) { return d.y;};
    	var yMin = d3.min(data, yValue);
    	var yMax = d3.max(data, yValue);
    	var mapY = options.secMap;
    	var lgY = options.secLinkageGroup;  
    	lgY = tripalMap.lGNameReduction(lgY, mapY);
    	var yLabel = [mapY, lgY,"0 -"+yMax+" cM"];

    	// y-axis
    	var yScale = d3.scaleLinear().range([height, 0]);
    	// prevent dots overlapping axis. Add buffer to data domain
    	yScale.domain([d3.min(data, yValue)-padding, d3.max(data, yValue) + padding]);
    	
    	var yAxis = '';
    	if (tripalMap.d3VersionFour()) { 
       		yAxis = d3.axisLeft(yScale);
       	}
       	else {
       		yAxis = d3.svg.axis().scale(yScale).orient("left");
       	 }
    	
    	
    	yAxisSvg = svg.append("g")
    		.attr("class", "y axis");
    	yAxisSvg.call(yAxis);
    	       		
       	var yAxisg = yAxisSvg.append("g")
       		.attr("fill", textColor)
    		.attr("transform", "rotate(-90)");

       	var yAxisgy = -70;
    	yAxisg.selectAll("text")
			.data(yLabel)
    		.enter().append("text")
    		.attr("class", "label").attr("x", -180).attr("y", function(d, i) { return (yAxisgy + (i*18));})
 			.style("text-anchor", "start").style("font-size", "15px").style("line-height", "18px")
    		.text(function(d) {return d;});
    	
    	// remove ticks on y and x axis
  	  	svg.selectAll(".tick").remove();

  	  	// wrap around text that is too long
		svg.selectAll(".label")
      			.call(tripalMap.wrap, 280);
 
        	// draw dots
    	var xMap = function(d) { return xScale(xValue(d));};
    	var yMap = function(d) { return yScale(yValue(d));};
    	svg.selectAll(".dot")
    		.data(data)
    		.enter().append("circle")
    			.attr("class", "dot")
    			.attr("r", 5)
    			.attr("cx", xMap)
    			.attr("cy", yMap)
    			.style("fill", textColor); 
        	
	},   
	
};


